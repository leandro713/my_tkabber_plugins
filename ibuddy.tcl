# $Id: ibuddy.tcl  2010-11-27  leandro $

#
# see pybuddy page for available commands (http://code.google.com/p/pybuddy/wiki/Commands)
#

namespace eval ibuddy {
    custom::defgroup Ibuddy \
	[::msgcat::mc " To use this module you need to buy an iBuddy, having installed and running on your machine Pybuddy (http://code.google.com/p/pybuddy/) and install TclUDP (http://sourceforge.net/projects/tcludp/)"] \
	-group Plugins \
	-tag "iBuddy manager"

    custom::defvar options(autoask) 0 \
	[::msgcat::mc "Use this module"] -group Ibuddy -type boolean
}

proc ibuddy::udp_puts {str args} {
        set s [udp_open]
        fconfigure $s -remote [list 127.0.0.1 8888]
        puts  $s $str
        close $s
}

proc ibuddy::udp_puts2 {str xlib from mid type is_subject subject body \
    err thread priority x} {
    	if {![string equal $type chat]} return
    	if { [string equal $body ""]}   return
    	set s [udp_open]
        fconfigure $s -remote [list 127.0.0.1 8888]
        puts  $s $str
        close $s
}

#each message event (by me) 
hook::add chat_send_message_hook [list [namespace current]::ibuddy::udp_puts "MACRO_GREEN"]

#each message event (by others)
hook::add process_message_hook   [list [namespace current]::ibuddy::udp_puts2 "MACRO_FLAP"]
hook::add process_message_hook   [list [namespace current]::ibuddy::udp_puts2 "MACRO_RED"]

#enter/exit chatroom 
hook::add chat_user_enter [list [namespace current]::ibuddy::udp_puts "MACRO_HEART2"]
hook::add chat_user_exit  [list [namespace current]::ibuddy::udp_puts "MACRO_HEART"]

#somebody connects or change his status 
hook::add connected_hook  [list [namespace current]::ibuddy::udp_puts "MACRO_LBLUE"]
